# IPL-Project

Download both csv files from https://www.kaggle.com/manasgarg/ipl

Coded a Java program that will transform the raw csv data into a data structure and covered following scenario :

*    Number of matches played per year of all the years in IPL.
*    Number of matches won of all teams over all the years of IPL.
*    For the year 2016 get the extra runs conceded per team.
*    For the year 2015 get the top economical bowlers.
*    Total runs scored by each players in IPL.
*    Total runs scored by each players in each year.

