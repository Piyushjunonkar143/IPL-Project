package src;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Main
{

    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int CITY = 2;
    public static final int DATE = 3;
    public static final int TEAM1 = 4;
    public static final int TEAM2 = 5;
    public static final int TOSS_WINNER = 6;
    public static final int TOSS_DECISION = 7;
    public static final int RESULT = 8;
    public static final int DL_APPLIED = 9;
    public static final int WINNER = 10;
    public static final int WIN_BY_RUNS = 11;
    public static final int WIN_BY_WICKETS = 12;
    public static final int PLAYER_OF_MATCH = 13;
    public static final int VENUE = 14;
    public static final int BATSMAN_RUNS = 15;
    public static final int EXTRA_RUNS = 16;
    public static final int TOTAL_RUNS = 17;
    public static final int PLAYER_DISMISSED = 18;

    public static void main(String[] args)
    {
        Main main = new Main();
        List<Delivery> deliveryList = main.getDeliveryList();
        List<Match> matchList = main.getMatchList();

        main.findNumberOfMatchesPlayedPerYear(matchList);
        main.findMatchesWonByTeam(matchList);
        main.findExtraRunsConcedePerTeamIn2016(deliveryList, matchList);
        main.findTopEconomicalBowler(deliveryList, matchList);
        main.findTotalRunsByEachPlayer(deliveryList);
        main.findTotalRunsByEachPlayersForEachYear(deliveryList, matchList);
    }

    public List<Match> getMatchList() {
        String line;
        List<Match> matchList = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("resource/matches.csv"));
            while ((line = reader.readLine()) != null) {
                String[] matchRow = line.split(",");
                Match match = new Match();

                match.setId(matchRow[ID]);
                match.setSeason(matchRow[SEASON]);
                match.setCity(matchRow[CITY]);
                match.setDate(matchRow[DATE]);
                match.setTeam1(matchRow[TEAM1]);
                match.setTeam2(matchRow[TEAM2]);
                match.setToss_winner(matchRow[TOSS_WINNER]);
                match.setToss_decision(matchRow[TOSS_DECISION]);
                match.setResult(matchRow[RESULT]);
                match.setDl_applied(matchRow[DL_APPLIED]);
                match.setWinner(matchRow[WINNER]);
                match.setWin_by_runs(matchRow[WIN_BY_RUNS]);
                match.setWin_by_wickets(matchRow[WIN_BY_WICKETS]);
                match.setPlayer_of_match(matchRow[PLAYER_OF_MATCH]);
                match.setVenue(matchRow[VENUE]);
                matchList.add(match);
            }
        } catch (IOException e) {
            System.out.println("something wrong happened while reading the matches file taking the input");
            e.printStackTrace();
        }
        return matchList;
    }

    public List<Delivery> getDeliveryList()
    {
        String line;
        List<Delivery> deliveryList = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader("resource/deliveries.csv"));
            while ((line = reader.readLine()) != null) {
                String[] deliveryRow = line.split(",", -1);
                Delivery delivery = new Delivery();

                delivery.setMatch_id(deliveryRow[ID]);
                delivery.setInning(deliveryRow[SEASON]);
                delivery.setBatting_team(deliveryRow[CITY]);
                delivery.setBowling_team(deliveryRow[DATE]);
                delivery.setOver(deliveryRow[TEAM1]);
                delivery.setBall(deliveryRow[TEAM2]);
                delivery.setBatsman(deliveryRow[TOSS_WINNER]);
                delivery.setNon_striker(deliveryRow[TOSS_DECISION]);
                delivery.setBowler(deliveryRow[RESULT]);
                delivery.setIs_super_over(deliveryRow[DL_APPLIED]);
                delivery.setWide_runs(deliveryRow[WINNER]);
                delivery.setBye_runs(deliveryRow[WIN_BY_RUNS]);
                delivery.setLegbye_runs(deliveryRow[WIN_BY_WICKETS]);
                delivery.setNoball_runs(deliveryRow[PLAYER_OF_MATCH]);
                delivery.setPenalty_runs(deliveryRow[VENUE]);
                delivery.setBatsman_runs(deliveryRow[BATSMAN_RUNS]);
                delivery.setExtra_runs(deliveryRow[EXTRA_RUNS]);
                delivery.setTotal_runs(deliveryRow[TOTAL_RUNS]);
                delivery.setPlayer_dismissed(deliveryRow[PLAYER_DISMISSED]);
                deliveryList.add(delivery);
            }
        } catch (IOException e) {
            System.out.println("something wrong happened while reading the deliveries files taking the input");
            e.printStackTrace();
        }
        return deliveryList;
    }

    public void findNumberOfMatchesPlayedPerYear(List<Match> matchList)
    {
        //1. Number of matches played per year of all the years in IPL.
        Map<String, Integer> seasonWithCountOfMatch = new TreeMap<>();
        Match match = matchList.get(1);
        int matchRow = 2, countOfMatch = 1;

        while (matchRow < matchList.size()) {
            String season = match.getSeason();
            match = matchList.get(matchRow++);
            if (match.getSeason().contains(season)) {
                seasonWithCountOfMatch.put(match.getSeason(), ++countOfMatch);
            } else {
                countOfMatch = 1;
            }
        }
        System.err.println("Number of matches played per year of all the years in IPL are : ");
        System.out.println(seasonWithCountOfMatch);
    }

    public void findMatchesWonByTeam(List<Match> matchList)
    {
        //2. Number of matches won of all teams over all the years of IPL.
        Map<String, Integer> teamWithCountOfMatchWon = new TreeMap<>();
        int matchRow = 1;

        while (matchRow < matchList.size()) {
            int countOfMatchesWon = 0;
            Match match = matchList.get(matchRow++);
            String winningTeam = match.getWinner();
            if (winningTeam.equals("")) {
                continue;
            }
            if (teamWithCountOfMatchWon.containsKey(winningTeam)) {
                countOfMatchesWon = teamWithCountOfMatchWon.get(winningTeam);
            }
            teamWithCountOfMatchWon.put(winningTeam, ++countOfMatchesWon);
        }
        System.err.println("Number of matches won of all teams over all the years of IPL.");
        System.out.println(teamWithCountOfMatchWon);
    }

    public void findExtraRunsConcedePerTeamIn2016(List<Delivery> deliveryList, List<Match> matchList)
    {
        //3. For the year 2016 get the extra runs conceded per team.
        Map<String, Integer> teamWithExtraRuns = new TreeMap<>();
        String battingTeam;
        int matchRow = 1;

        while (matchRow < matchList.size()) {
            Match match = matchList.get(matchRow++);
            int deliveryRow = 1;
            if (match.getSeason().equals("2016")) {
                while (deliveryRow < deliveryList.size()) {
                    Delivery delivery = deliveryList.get(deliveryRow++);
                    battingTeam = delivery.getBatting_team();
                    int countOfExtraRun;
                    if (delivery.getMatch_id().equals(match.getId())) {
                        if (teamWithExtraRuns.containsKey(battingTeam) && delivery.getExtra_runs() != null) {
                            countOfExtraRun = teamWithExtraRuns.get(battingTeam)
                                    + Integer.parseInt(delivery.getExtra_runs());
                        } else {
                            countOfExtraRun = Integer.parseInt(delivery.getExtra_runs());
                        }
                        teamWithExtraRuns.put(battingTeam, countOfExtraRun);
                    }
                }
            }
        }
        System.err.println("For the year 2016 get the extra runs conceded per team are :.");
        System.out.println(teamWithExtraRuns);
    }

    public void findTopEconomicalBowler(List<Delivery> deliveryList, List<Match> matchList)
    {
        //4. For the year 2015 get the top economical bowlers.
        Map<String, Integer> bowlerNameWithTotalRuns = new TreeMap<>();
        Map<String, Integer> bowlerNameWithTotalBall = new TreeMap<>();
        Map<String, Double> bowlerNameWithEconomy = new TreeMap<>();
        int totalRuns, countOfBalls;
        String bowlerName;
        int matchRow = 1;

        while (matchRow < matchList.size()) {
            Match match = matchList.get(matchRow++);
            int deliveryRow = 0;
            if (match.getSeason().equals("2015")) {
                while (deliveryRow < deliveryList.size()) {
                    Delivery delivery = deliveryList.get(deliveryRow++);
                    bowlerName = delivery.getBowler();
                    if (delivery.getMatch_id().equals(match.getId())) {
                        if (bowlerNameWithTotalRuns.containsKey(bowlerName)) {
                            totalRuns = bowlerNameWithTotalRuns.get(bowlerName)
                                         + Integer.parseInt(delivery.getTotal_runs());
                            countOfBalls = bowlerNameWithTotalBall.get(bowlerName) + 1;
                        } else {
                            totalRuns = 0;
                            countOfBalls = 1;
                        }
                        bowlerNameWithTotalRuns.put(bowlerName, totalRuns);
                        bowlerNameWithTotalBall.put(bowlerName, countOfBalls);
                        bowlerNameWithEconomy.put(bowlerName, totalRuns / (countOfBalls / 6.0));
                    }
                }
            }
        }

        double minEconomyRate = 100;
        String minEconomyBowler = null;
        for (String bowler : bowlerNameWithEconomy.keySet()) {
            double value = bowlerNameWithEconomy.get(bowler);
            if (value < minEconomyRate) {
                minEconomyRate = value;
                minEconomyBowler = bowler;
            }
        }
        System.err.println("Most Economical bowler is : ");
        System.out.println(minEconomyBowler + " with economy rate " + minEconomyRate);
    }

    public void findTotalRunsByEachPlayer(List<Delivery> deliveryList)
    {
        //5. Total runs scored by each players in IPL
        Map<String, Integer> playerWithTotalRuns = new TreeMap<>();
        int score;

        for (int deliveryRow = 1; deliveryRow < deliveryList.size(); deliveryRow++) {
            Delivery delivery = deliveryList.get(deliveryRow);
            String batsmanName = delivery.getBatsman();

            if (playerWithTotalRuns.containsKey(batsmanName)) {
                score = Integer.parseInt(delivery.getTotal_runs())
                        + playerWithTotalRuns.get(batsmanName);
            } else {
                score = Integer.parseInt(delivery.getTotal_runs());
            }
            playerWithTotalRuns.put(batsmanName, score);
        }
        System.err.println("Total runs scored by each players in IPL");
        System.out.println(playerWithTotalRuns);
    }

    public void findTotalRunsByEachPlayersForEachYear(List<Delivery> deliveryList, List<Match> matchList)
    {
        //6. Total Runs Scored by each player for each year
        Map<Integer, Map<String, Integer>> seasonWithPlayers = new TreeMap<>();
        Map<String, Integer> batsmanWithRuns;

        for (int matchRow = 1; matchRow < matchList.size(); matchRow++) {
            String matchId = matchList.get(matchRow).getId();
            batsmanWithRuns = new TreeMap<>();

            for (int deliveryRow = 1; deliveryRow < deliveryList.size(); deliveryRow++) {
                if (matchId.equals(deliveryList.get(deliveryRow).getMatch_id())) {
                    Delivery delivery = deliveryList.get(deliveryRow);
                    String batsmanName = delivery.getBatsman();
                    int scoreByBatsman;

                    if (batsmanWithRuns.containsKey(batsmanName)) {
                        scoreByBatsman = Integer.parseInt(delivery.getTotal_runs())
                                + batsmanWithRuns.get(batsmanName);
                    } else {
                        scoreByBatsman = Integer.parseInt(delivery.getTotal_runs());
                    }
                    batsmanWithRuns.put(batsmanName, scoreByBatsman);
                }
            }
            seasonWithPlayers.put(Integer.valueOf(matchList.get(matchRow).getSeason()), batsmanWithRuns);
        }
        System.err.println("Total runs scored by each players in each year");
        System.out.println(seasonWithPlayers);
    }
}


